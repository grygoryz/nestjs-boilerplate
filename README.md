# NestJS Boilerplate

## Уже сделано
- NestJS
- TypeORM
- Миграции и сидеры
- CI/CD template
- Git hooks (lint-staged, install-deps-postmerge)
- Логирование
- Swagger автогенерация

## Package.json

Скрипты взяты из [официального NestJS стартера](https://github.com/nestjs/typescript-starter), но также есть другие:

- `typeorm:schema:drop` - дропнуть схему бд (удаляет все обьекты БД вместе с данными)
- `typeorm:migration:generate` - сгенерировать новую миграцию на основе изменений typeorm entities. Принимает в качестве аргумента имя миграции, например: `npm run typeorm:migration:generate create-users-table`
- `typeorm:migration:run` - применить все миграции, которые не были применены
- `typeorm:migration:revert` - отменить последнюю миграцию
- `typeorm:seed:run` - запустить все сидеры
- `typeorm:seed:run:single` - запустить конкретный сидер. В качестве аргумента имя класса сидера, например: `npm run typeorm:seed:run:single RolesSeeder`

## Описание архитектуры
Стандартная архитектура NestJS, но есть некоторые детали:
- Сущности request-dtos, response-dtos, bos. 
  - request-dtos - нужны для типизации входных в контроллеры данных, их валидации и отражении в swagger доке.
Касаемо обычных REST контроллеров, входные данные могут быть в параметрах, теле или query, поэтому следует указывать
это в названии классов, например: файл request-dtos/get-user.dto.ts с классами GetUserDTOParams, GetUserDTOBody, GetUserDTOQuery.
  - response-dtos - нужны для типизации возвращаемых значений контроллеров и их отражении в swagger доке.
  - bos (Business Objects) - нужны для типизации различных сущностей в сервисах (возвращаемые значения, параметры и тд).
- Для вызова методов сервиса одного модуля в другом модуле, следует экспортировать не сам сервис из модуля, а его Фасад (пример в данном репозитории по пути src/users/users-facade.service.ts)

## Структура проекта

* `src/`
  * `common/` - общие для всех модулей сущности (decorators, interceptors, pipes, bos, exceptions, request-dtos, middlewares и тд)
  * `config/` - конфигурация
      * `configurations.ts` - здесь маппятся все переменные окружения, которые потом передаются в nest 
      * `config.interface.ts` - типизация переменных окружения
      * _также здесь содержатся конфиги для различных модулей (logger.config.ts, redis.config.ts и тд)_
  * `databases/` - файлы, касающиеся различных БД
      * `postgres/`
        * `entities/` - typeorm entities
        * `migrations/` - миграции
        * `seeds/` - сидеры
  * `helpers/` - хелперы (утилиты, константы и тд)
  * `ormconfig.ts` - конфиг typeorm
  * `custom.d.ts` - typescript declarations. Здесь можно переопределять/дополнять типизацию встроенных/сторонних модулей.
  * `app.module.ts` - корневой модуль приложения
  * `main.ts` - точка входа приложения
* `.env` - переменные окружения, пробрасываемые в **режиме разработки**
* `docker-compose.yml` - описание окружения для разработки

## Как поднять для разработки
Окружение для разработки нужно описывать в docker-compose.yml. Следовательно, алгоритм такой:
1. `docker-compose up`
2. `npm run start:dev`

Сервер запустится на порту PORT, указанном в .env.

## Миграции и сидеры
Все изменения объектов БД (таблицы, индексы) осуществляются исключительно через миграции. Для работы с 
миграциями есть соответствующие скрипты из package.json (typeorm:migration:*). При генерации миграций следует
проверять выданный SQL. Для полного сброса БД есть скрипт `typeorm:schema:drop`, однако нужно следить, чтобы
случайно не применить его на сервере деплоя.

На задеплоенном инстансе миграции следует применять мануально:
1. Зайти на сервер, найти id докер контейнера сервера через docker ps.
2. Выполнить `docker exec -t {container_id} npm run typeorm:migration:run`

Механизм сидеров реализован через библиотеку [typeorm-seeding](https://www.npmjs.com/package/typeorm-seeding). При запуске
всех сидеров (`typeorm:seed:run`) они запускаются в алфавитном порядке по названию файлов, поэтому для управления порядком нужно называть
файлы в таком формате: 1-user.ts, 2-roles.ts и тд. 

Следует помнить, что сидеры не предполагают механизма отката как миграции. Аналогично миграциям, сидеры
на задеплоенном инстансе нужно запускать мануально через `docker exec`.


## Логирование
Логирование реализовано с помощью [pino](https://www.npmjs.com/package/pino) ([nestjs-pino](https://www.npmjs.com/package/nestjs-pino), [pino-http](https://www.npmjs.com/package/pino-http)). 
Все http запросы автоматически логируются. Сам Logger следует использовать [как показано](https://docs.nestjs.com/techniques/logger#using-the-logger-for-application-logging) в доке NestJS (импортить Logger из @nestjs/common!).
Конфигурация логгера находится в src/config/logger.config.ts.

Для логирования тел ответов нужно передать переменную окружения LOG_RESPONSE_BODY=true, а в модулях нужно подключить
специальную мидлвару attachResponseBodyToRes к нужным контроллерам следующим способом:
```
export class UsersModule implements NestModule {
constructor(private configService: ConfigService<ConfigVariables>) {}

	configure(consumer: MiddlewareConsumer) {
		if (this.configService.get('isResponseBodyLoggingEnabled', { infer: true })) {
			consumer.apply(attachResponseBodyToRes).forRoutes(UsersController);
		}
	}
}
```

## Error handling
Из сервисов **запрещается** выбрасывать ошибки, импортированные из @nestjs/common: HttpException, ForbiddenException
и тд, так как данные ошибки относятся исключительно к http, а сервисы не должны ничего знать о транспорте.

Если код ошибки не важен, то из сервиса можно выкинуть просто `throw new Error('some message')`, далее эту ошибку
поймает глобальный фильтр и отправит в ответ на запрос 500 ошибку с заданным сообщением в теле.
Если же при возникновении ошибки необходимо, чтобы она отправилась с конкретным статусом, то предлагается создать
кастомный exception _(см. src/users/exceptions/user-data-not-found.exception.ts)_ и ловить его на уровне контроллера 
с помощью интерцептора ErrorMappingInterceptor, преобразовывая к нужной ошибке:

`@UseInterceptors(new ErrorMappingInterceptor({ from: UserDataNotFoundException, to: () => new NotFoundException() }))`

_(см. src/users/users.controller.ts)_ 

## Swagger документация
Автогенерация настроена как в [доке nestjs](https://docs.nestjs.com/openapi/introduction). Обязательно нужно типизировать
вход/выход контроллеров с помощью DTO (request-dtos, response-dtos).

По дефолту дока открывается по пути '/api', но это можно изменить в main.ts.


## CI/CD
Настроен CI/CD для деплоя на дев. Перечень необходимых переменных окружения можно посмотреть в интерфейсе gitlab: Settings -> CI/CD -> Variables.
Там есть две переменные портов:
- APP_PORT - порт, который займет сервер
- DB_PORT - порт, который займет БД, чтобы к ней можно было подключиться через клиент (Datagrip, например).


## Другое
- В проекте настроены tsconfig paths таким образом, чтобы не нужно было их конфигурировать при добавлении новых
модулей. Это достигается простым маппингом 'src' к '@', получаются такие пути:
```
import { UsersModule } from '@/users/users.module';
import { ErrorMappingInterceptor } from '@/common/interceptors';
import { DeleteUserDTOParams, GetUserDTOParams } from '@/users/request-dtos';
```
- Названия всех объектов в postgres должны быть в snake_case. Для названий из двух и более слов в TypeORM следует
указывать имя в snake_case (ключ name):
```
@Column('varchar', { length: 255, name: 'first_name' })
firstName!: string;
```
- Следует явно записывать название таблицы при описании TypeORM entity:
```
@Entity({ name: 'users' })
```


