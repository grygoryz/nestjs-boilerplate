import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'users' })
export class User {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column('varchar', { length: 255 })
	name!: string;
}
