declare global {
	interface Error {
		name: string;
		message: string;
		stack?: string;
		originError?: Error;
	}
}

export {};
