import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { catchError, Observable, throwError } from 'rxjs';

interface ErrorMapping {
	from: new (message?: string) => Error;
	to: (message?: string) => Error;
}

@Injectable()
export class ErrorMappingInterceptor implements NestInterceptor {
	private readonly errorsMappings: Array<ErrorMapping>;

	constructor(...args: Array<ErrorMapping>) {
		this.errorsMappings = args;
	}

	intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
		return next.handle().pipe(
			catchError(error => {
				for (const { from, to } of this.errorsMappings) {
					if (error instanceof from) {
						const newError = to(error.message);
						newError.originError = error;
						return throwError(() => newError);
					}
				}

				return throwError(() => error);
			}),
		);
	}
}
