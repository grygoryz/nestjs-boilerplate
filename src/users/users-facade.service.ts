import { Injectable } from '@nestjs/common';
import { UserBO } from '@/users/bos';
import { UsersService } from '@/users/users.service';

@Injectable()
export class UsersFacadeService {
	constructor(private readonly usersService: UsersService) {}

	async getUser(id: number): Promise<UserBO> {
		return await this.usersService.getUser(id);
	}
}
