import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersFacadeService } from './users-facade.service';
import { User } from '@/databases/postgres/entities';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ConfigVariables } from '@/config';
import { attachResponseBodyToRes } from '@/common/middlewares';

@Module({
	imports: [TypeOrmModule.forFeature([User]), ConfigModule],
	controllers: [UsersController],
	providers: [UsersService, UsersFacadeService],
	exports: [UsersFacadeService],
})
export class UsersModule implements NestModule {
	constructor(private configService: ConfigService<ConfigVariables>) {}

	configure(consumer: MiddlewareConsumer) {
		if (this.configService.get('isResponseBodyLoggingEnabled', { infer: true })) {
			consumer.apply(attachResponseBodyToRes).forRoutes(UsersController);
		}
	}
}
