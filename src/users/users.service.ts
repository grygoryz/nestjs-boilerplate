import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '@/databases/postgres/entities';
import { UserDataNotFoundException } from '@/users/exceptions';
import { UserBO } from '@/users/bos';

@Injectable()
export class UsersService {
	constructor(@InjectRepository(User) private readonly userRepository: Repository<User>) {}

	async getUser(id: number): Promise<UserBO> {
		const user = await this.userRepository.findOne(id);
		if (!user) {
			throw new UserDataNotFoundException();
		}

		return user;
	}

	async deleteUser(id: number): Promise<void> {
		const result = await this.userRepository.delete(id);
		if (result.affected !== 1) {
			throw new Error('Child not found');
		}
	}
}
