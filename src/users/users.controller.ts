import { Controller, Delete, Get, NotFoundException, Param, UseInterceptors } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GetUserDTO } from './response-dtos';
import { UserDataNotFoundException } from '@/users/exceptions';
import { ErrorMappingInterceptor } from '@/common/interceptors';
import { DeleteUserDTOParams, GetUserDTOParams } from '@/users/request-dtos';

@Controller('users')
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@Get(':id')
	@ApiTags('User')
	@ApiOperation({ summary: 'get user profile' })
	@ApiOkResponse({ type: GetUserDTO })
	@UseInterceptors(new ErrorMappingInterceptor({ from: UserDataNotFoundException, to: () => new NotFoundException() }))
	async getUser(@Param() params: GetUserDTOParams): Promise<GetUserDTO> {
		return await this.usersService.getUser(params.id);
	}

	@Delete('/users/:userId')
	@ApiTags('User')
	@ApiOperation({ summary: 'delete user' })
	async deleteUser(@Param() params: DeleteUserDTOParams): Promise<void> {
		return await this.usersService.deleteUser(params.id);
	}
}
