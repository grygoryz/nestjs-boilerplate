import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { Type } from 'class-transformer';

export class DeleteUserDTOParams {
	@ApiProperty()
	@IsInt()
	@Type(() => Number)
	id!: number;
}
